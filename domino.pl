dominos([(0,0),(0,1),(0,2),(0,3),(0,4),(0,5),(0,6),
               (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),
                     (2,2),(2,3),(2,4),(2,5),(2,6),
                           (3,3),(3,4),(3,5),(3,6),
                                  (4,4),(4,5),(4,6),
                                        (5,5),(5,6),
                                              (6,6)]).

frame([[3,1,2,6,6,1,2,2],
       [3,4,1,5,3,0,3,6],
       [5,6,6,1,2,4,5,0],
       [5,6,4,1,3,3,0,0],
       [6,1,0,6,3,2,4,0],
       [4,1,5,2,4,3,5,5],
       [4,1,0,2,4,5,2,0]]).

/* Κάποια σχόλια για τον κώδικα: Στην αρχή βρίσκουμε τις πιθανές θέσεις που μπορεί να μπει κάθε
   domino. Στην συνέχεια εφαρμόζουμε τον mrv και διαγράφουμε τα 'δεσμευμένα' κελιά και από τα υπόλοιπα
   domino. Επίσης υπάρχει μια λίστα Blocked που εμποδίζει να εκτυπωθούν ίδιες λύσεις(στο μάτι) ανεξαρτήτως
   αν υπάρχουν δύο διακριτά domino με ίδιες τιμές.
*/

mylengthAcc([], Len, Len).
mylengthAcc([_|T], Len, Acc) :- Acc_1 is Acc+1, mylengthAcc(T, Len, Acc_1).

mylength(List, Len) :- mylengthAcc(List, Len, 0).

myappend([], L, L).
myappend([H1|T1], L2, [H1|T]) :- myappend(T1, L2, T).

mymember([H|_], H) :- !.
mymember([_|T], X) :- mymember(T, X).

hasEmpty([[]|_]) :- !.
hasEmpty([_|T]) :- hasEmpty(T).

removeByPos([_|T], Goal, Goal, T) :- !. 
removeByPos([H|T], Goal, Cnt, [H|L]) :- Cnt_1 is Cnt+1, removeByPos(T, Goal, Cnt_1, L).

boardHeight(L, Len) :- mylength(L, Len).
boardWidth([H|_], Len) :- mylength(H, Len).

existInto([L1,_], Cell) :- L1 == Cell.
existInto([_,L2], Cell) :- L2 == Cell.

/* Get element by index */
/* ------------------------------------------------------------------- */

getColumnElem([H|_], C, C, _, H) :- !.
getColumnElem([_|T], C, Goal, Wdt, Elem) :- C1 is C+1, C1 < Wdt, getColumnElem(T, C1, Goal, Wdt, Elem).

getRowElem([H|_], R, (R,C), _, Wdt, Elem) :- getColumnElem(H, 0, C, Wdt, Elem), !.
getRowElem([_|F], R, Goal, Hgt, Wdt, Elem) :- R1 is R+1, R1 < Hgt, getRowElem(F, R1, Goal, Hgt, Wdt, Elem).

getElem(F, Goal, Hgt, Wdt, Elem) :- getRowElem(F, 0, Goal, Hgt, Wdt, Elem).


getRight(D, F, (Row,Col), Hgt, Wdt, [[(Row,Col),(Row,Col_1)]]) :- getElem(F, (Row,Col), Hgt, Wdt, E1), Col_1 is Col+1, Col_1 < Wdt, 
                                                                  getElem(F, (Row,Col_1), Hgt, Wdt, E2), D == (E1, E2), !.

getRight(_, _, _, _, _, []).

getDown(D, F, (Row,Col), Hgt, Wdt, [[(Row,Col),(Row_1,Col)]]) :- getElem(F, (Row,Col), Hgt, Wdt, E1), Row_1 is Row+1, Row_1 < Hgt, 
                                                                 getElem(F, (Row_1,Col), Hgt, Wdt, E2), D == (E1, E2), !.

getDown(_, _, _, _, _, []).

getDominoByPos([H|_], Goal, Goal, H) :- !.
getDominoByPos([_|T], Goal, Pos, Dom) :- Pos_1 is Pos+1, getDominoByPos(T, Goal, Pos_1, Dom).

getByPos([H|_], Goal, Goal, H) :- !.
getByPos([_|T], Goal, Pos, X) :- Pos_1 is Pos+1, getByPos(T, Goal, Pos_1, X).

getFirstN([_|_], Goal, Pos, []) :- Goal < Pos, !.
getFirstN([H|_], Goal, Goal, [H]) :- !.
getFirstN([H|T], Goal, Pos, [H|L]) :- Pos_1 is Pos+1, getFirstN(T, Goal, Pos_1, L).

/* ------------------------------------------------------------------- */

/* Find possible positions of dominos */
/* ------------------------------------------------------------------- */

findPos(_, _, (Hgt,_), Hgt, _, L, L) :- !.
findPos(D, F, (Row,Wdt), Hgt, Wdt, L, Acc) :- Row_1 is Row+1, findPos(D, F, (Row_1,0), Hgt, Wdt, L, Acc), !.
findPos(D, F, (Row,Col), Hgt, Wdt, L, Acc) :- getRight(D, F, (Row,Col), Hgt, Wdt, Rgt), getDown(D, F, (Row,Col), Hgt, Wdt, Down),
                                              myappend(Rgt, Down, Temp), myappend(Temp, Acc, Acc_1),
                                              Col_1 is Col+1, findPos(D, F, (Row,Col_1), Hgt, Wdt, L, Acc_1), !.


findPositions([], _, _, _, []).
findPositions([(D1,D2)|D], F, Hgt, Wdt, [L|T]) :- D1 == D2, !,
                                                  findPos((D1,D2), F, (0,0), Hgt, Wdt, L, []), 
                                                  findPositions(D, F, Hgt, Wdt, T).

findPositions([(D1,D2)|D], F, Hgt, Wdt, [L|T]) :- findPos((D1,D2), F, (0,0), Hgt, Wdt, L1, []), 
                                                  findPos((D2,D1), F, (0,0), Hgt, Wdt, L2, []),
                                                  myappend(L1, L2, L),
                                                  findPositions(D, F, Hgt, Wdt, T).

/* ------------------------------------------------------------------- */

initBlock(Goal, Goal, []) :- !.
initBlock(Goal, Pos, [[]|T]) :- Pos_1 is Pos+1, initBlock(Goal,Pos_1, T). 

blocked(C, [(D1,D2)|_], (D1,D2), [BH|_]) :- mymember(BH, C), !. 
blocked(C, [(D2,D1)|_], (D1,D2), [BH|_]) :- mymember(BH, C), !. 
blocked(C, [_|DT], Dom, [_|BT]) :- blocked(C, DT, Dom, BT). 

getNonBlockedElements([H|_], D, Dom, Block, H, Pos, Pos) :- not(blocked(H, D, Dom, Block)). 
getNonBlockedElements([_|T], D, Dom, Block, Elem, Cnt, Pos) :- Cnt_1 is Cnt+1, getNonBlockedElements(T, D, Dom, Block, Elem, Cnt_1, Pos).

insertBlock([], [], _, _, []).
insertBlock([H|T], [(D1,D2)|_], (D1,D2), FB, [H1|T]) :- !, myappend(H, FB, H1).
insertBlock([H|T], [(D2,D1)|_], (D1,D2), FB, [H1|T]) :- !, myappend(H, FB, H1).
insertBlock([H|T], [_|DT], Dom, FB, [H|L]) :- insertBlock(T, DT, Dom, FB, L).

minLen([], Min, Min).
minLen([H|T], Min, Acc) :- mylength(H, L), Acc =:= -1, minLen(T, Min, L), !.    
minLen([H|T], Min, Acc) :- mylength(H, L), L < Acc, minLen(T, Min, L), !.
minLen([_|T], Min, Acc) :- minLen(T, Min, Acc).

minPos([H|_], Min, Pos, Pos) :- mylength(H, L), L =:= Min, !.
minPos([_|T], Min, Cnt, Pos) :- Cnt_1 is Cnt+1, minPos(T, Min, Cnt_1, Pos).

mrv([H|_], Goal, Goal, D, Dom, Block, Loc, Until) :- !, getNonBlockedElements(H, D, Dom, Block, Loc, 0, Until).
mrv([_|T], Goal, Pos, D, Dom, Block, Loc, Until) :- Pos_1 is Pos+1, mrv(T, Goal, Pos_1, D, Dom, Block, Loc, Until).

/* Remove used cells */
/* ------------------------------------------------------------------- */

removeSingle([], _, _, []).
removeSingle([H|T], Cell_1, Cell_2, P) :- existInto(H, Cell_1), removeSingle(T, Cell_1, Cell_2, P), !.
removeSingle([H|T], Cell_1, Cell_2, P) :- existInto(H, Cell_2), removeSingle(T, Cell_1, Cell_2, P), !. 
removeSingle([H|T], Cell_1, Cell_2, [H|P]) :- removeSingle(T, Cell_1, Cell_2, P). 

remove([], _, _, []).
remove([H|T], Cell_1, Cell_2, [Prem|P]) :- removeSingle(H, Cell_1, Cell_2, Prem), remove(T, Cell_1, Cell_2, P).  

/* ------------------------------------------------------------------- */

solve([], _, [], _, []).
solve(P, Dst, Ddy, Block, [[Cell_1,Cell_2]|S]) :- minLen(P, Min, -1), minPos(P, Min, 0, Pos), getDominoByPos(Ddy, Pos, 0, Dom),
                                                  mrv(P, Pos, 0, Dst, Dom, Block, [Cell_1,Cell_2], Until), 
                                                  getByPos(P, Pos, 0, L), getFirstN(L, Until, 1, FB),
                                                  insertBlock(Block, Dst, Dom, FB, Block_1), 
                                                  remove(P, Cell_1, Cell_2, P1), removeByPos(P1, Pos, 0, P2), 
                                                  removeByPos(Ddy, Pos, 0, Ddy_1), not(hasEmpty(P2)),
                                                  solve(P2, Dst, Ddy_1, Block_1, S).

solveBoard(D, F, Hgt, Wdt, S) :- findPositions(D, F, Hgt, Wdt, P), 
                                 mylength(D, Len), initBlock(Len, 0, Block), 
                                 solve(P, D, D, Block, S).

/* Show result */
/* ------------------------------------------------------------------- */

show(_, _, (R,_), Hgt, _) :- R > 2*(Hgt-1), !.

show(F, S, (R,C), Hgt, Wdt) :- C > 2*(Wdt-1), R1 is R+1, write('\n'), !, show(F, S, (R1,0), Hgt, Wdt).

show(F, S, (R,C), Hgt, Wdt) :- mod(R, 2) =:= 0, mod(C, 2) =:= 0, R1 is div(R,2), C1 is div(C,2),
                               getElem(F, (R1,C1), Hgt, Wdt, E), write(E), !,
                               C2 is C+1, show(F, S, (R,C2), Hgt, Wdt).

show(F, S, (R,C), Hgt, Wdt) :- mod(R, 2) =:= 0, R1 is div(R,2), C1 is div(C,2), C2 is C1+1,
                               mymember(S, [(R1,C1),(R1,C2)]), write('-'), !,
                               C3 is C+1, show(F, S, (R,C3), Hgt, Wdt).

show(F, S, (R,C), Hgt, Wdt) :- mod(R, 2) =:= 0, write(' '), !, C1 is C+1,
                               show(F, S, (R,C1), Hgt, Wdt).

show(F, S, (R,C), Hgt, Wdt) :- mod(C, 2) =:= 0, R1 is div(R,2), C1 is div(C,2), R2 is R1+1,
                               mymember(S, [(R1,C1),(R2,C1)]), write('|'), !,
                               C2 is C+1, show(F, S, (R,C2), Hgt, Wdt).

show(F, S, (R,C), Hgt, Wdt) :- write(' '), !, C1 is C+1, show(F, S, (R,C1), Hgt, Wdt).

/* ------------------------------------------------------------------- */

put_dominos :- dominos(D), frame(F), boardHeight(F, Hgt), boardWidth(F, Wdt),
               solveBoard(D, F, Hgt, Wdt, S),
               show(F, S, (0,0), Hgt, Wdt).
